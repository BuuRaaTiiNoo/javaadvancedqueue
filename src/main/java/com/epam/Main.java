package com.epam;


import com.epam.DTO.Consumer;
import com.epam.DTO.Producer;

public class Main {

    public static void main(String[] args) {

        TQueue tQueue = new TQueue();
        Producer producer = new Producer(tQueue);
        Consumer consumer = new Consumer(tQueue);

        new Thread(producer).start();
        new Thread(producer).start();
        new Thread(producer).start();

        new Thread(consumer).start();
        new Thread(consumer).start();
        new Thread(consumer).start();
        new Thread(consumer).start();
        new Thread(consumer).start();
        new Thread(consumer).start();
    }
}
