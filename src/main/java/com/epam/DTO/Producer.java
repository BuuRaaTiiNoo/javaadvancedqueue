package com.epam.DTO;

import com.epam.TQueue;

public class Producer implements Runnable {

    private final TQueue tQueue;

    public Producer(TQueue tQueue) {
        this.tQueue = tQueue;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            tQueue.put();
        }
    }
}
