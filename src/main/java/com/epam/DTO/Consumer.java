package com.epam.DTO;

import com.epam.TQueue;
import org.apache.log4j.Logger;

public class Consumer implements Runnable {

    private final TQueue tQueue;

    public Consumer(TQueue tQueue) {
        this.tQueue = tQueue;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Logger.getLogger(Consumer.class).error("Error: ", e);
            }
            tQueue.get();
        }
    }
}