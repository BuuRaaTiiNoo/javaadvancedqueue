package com.epam.DTO;

import java.time.LocalDate;

public class HotelBookingRequests {

    private String hotelName;
    private LocalDate localDate;

    public HotelBookingRequests(String hotelName, LocalDate localDate) {
        this.hotelName = hotelName;
        this.localDate = localDate;
    }

    public HotelBookingRequests() {
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    @Override
    public String toString() {
        return "hotelName: " + hotelName + " localDate: " + localDate;
    }
}
