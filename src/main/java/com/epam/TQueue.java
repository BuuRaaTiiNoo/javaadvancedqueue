package com.epam;

import com.epam.DTO.HotelBookingRequests;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class TQueue {

    private static final Logger logger = Logger.getLogger(TQueue.class);
    private static final int MAX_CAPACITY = 5;
    private static final int TOTAL_NUMBER_OF_REQUEST = 15;
    private volatile int countPut = 0;
    private volatile int countGet = 0;


    private List<HotelBookingRequests> arrayList = new ArrayList<>();

    public synchronized void get() {
        while (arrayList.isEmpty()) {
            if (countGet != TOTAL_NUMBER_OF_REQUEST) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    logger.error("Error: ", e);
                }
            } else {
                Thread.currentThread().interrupt();
                return;
            }
        }
        if (countGet < TOTAL_NUMBER_OF_REQUEST) {
            arrayList.remove(arrayList.size() - 1);
            logger.info("Производитель выполнил запрос");
            notifyAll();
            countGet++;
        } else Thread.currentThread().interrupt();
    }

    public synchronized void put() {
        if (countPut < TOTAL_NUMBER_OF_REQUEST) {
            while (arrayList.size() >= MAX_CAPACITY) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    logger.error("Error: ", e);
                }
            }
            if (countPut < TOTAL_NUMBER_OF_REQUEST) {
                arrayList.add(new HotelBookingRequests());
                logger.info("Потребитель создал запрос");
                notifyAll();
                countPut++;
            }
        } else Thread.currentThread().interrupt();
    }
}
